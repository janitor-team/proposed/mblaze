mblaze (1.1-1) unstable; urgency=medium

  * New upstream version (2021-01-14)
    + New utility mmailto
    + Improvements to mcom
    + Bug fixes

  * Amend v1.0-1 changelog, mention setting up Salsa CI
  * d/patches: Refresh for new upstream release
  * d/control
    + Declare compliance with policy v4.5.1.
    + Upgrade to debhelper compatibility level 13

 -- nicoo <nicoo@debian.org>  Thu, 14 Jan 2021 17:10:53 +0100

mblaze (1.0-1) unstable; urgency=low

  * New upstream release (2020-09-12)
    + Backwards incompatible changes:
      - As a message name, `-` now refers to the message on the standard input,
        and not the previous message anymore.  Use `.-` to refer to
        the previous message in a short way.
        The tools will print a warning if you use `-` and the standard input
        comes from a TTY.
      - mpick: use the -F flag to read script files.
      - mpick: remove msglist support, use plain mmsg(7) arguments.
    + Add contrib/mcount, a tool to count mails
    + All tools now follow symlinks to mails.
    + Many bug fixes.

  * Rename the packaging branch to debian/sid
  * Update package authorship information
  * Update upstream's canonical URL (on vuxu.or)

  * Setup Salsa CI for the packaging repository
  * d/source: Remove Lintian override for obsoleted tag
  * d/patches: Fix DEP-3 metadata format

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 10:45:35 +0100

mblaze (0.6-1) unstable; urgency=medium

  * New upstream release (2020-01-18)
    + mscan: Add dottime formatting
    + mlist: Check for messages in maildir/new

  * debian/watch: Use git mode, check upstream's signature
  * Declare compliance with policy v4.5.0.
    No change required

 -- nicoo <nicoo@debian.org>  Thu, 30 Jan 2020 20:01:01 +0100

mblaze (0.5.1-1) unstable; urgency=high (security fixes)

  * New upstream release (2019-03-03)
    + Fixes for buffer-overflows, found by fuzzing.
    + Fixes for memleaks.

  * Remove unused lintian override
  * debian/rules: Remove useless dh_missing invocation.
    A single binary package is being produced.

 -- nicoo <nicoo@debian.org>  Mon, 04 Mar 2019 18:40:01 +0100

mblaze (0.5-1) unstable; urgency=medium

  * New upstream release (2019-02-09)
    + Closes: 921891
    + GNUmakefile: correct reference to SOURCE_DATE_EPOCH, patch by Chris Lamb
      Closes: #907537 

  * Rename mless and msort to mblaze-less and mblaze-sort
    Closes: #887988 correctly, avoids conflicts on mpdtoys & msort

  * Switch to debhelper 12.
    The compatibility level is now controlled by a Build-Depends.

  * Declare compliance with policy v4.2.1.
    No change required.

  * debian/rules
    + Make dh_missing fail the build
    + Symlink docs from mblaze-* to mblaze

 -- nicoo <nicoo@debian.org>  Tue, 12 Feb 2019 11:44:33 +0100

mblaze (0.4-1) unstable; urgency=medium

  * New upstream version (2018-08-15)
    + Adds a new utility: mrefile
    + Remove obsolete Debian patches.
      Upstream now obeys SOURCE_DATE_EPOCH.

  * Declare compliance with policy 4.2.0 (no change needed)
  * Use the more-precise Expat license name (rather than MIT)
  * Make git-buildpackage commit generated tarballs to pristine-tar

 -- nicoo <nicoo@debian.org>  Sun, 19 Aug 2018 12:22:49 +0200

mblaze (0.3.2-2) unstable; urgency=low

  * Apply reproducible build patch (Closes: 890551)
  * Move the packaging repository to salsa.d.o
  * Declare compliance with policy v4.1.4.
    No change needed.
  * debian/rules
    - Drop unnecessary --parallel dh argument
    - Remove a superfluous use of dh-exec

 -- nicoo <nicoo@debian.org>  Wed, 09 May 2018 22:02:18 +0200

mblaze (0.3.2-1) unstable; urgency=high

  * New upstream version (2018-02-13)
    * Fix of a buffer overflow in blaze822_multipart.
    * magrep: add *:REGEX to search in any header
    * Small bug fixes.
    * Many documentation improvements by Larry Hynes.

  * debian/source/lintian-overrides
    * Relocate from debian/source.lintian-overrides
    * Rename debian-watch-may-check-gpg-signature
    * Add override_dh_auto_test-does-not-check-DEB_BUILD_PROFILES
      The tests are currently disabled.

 -- nicoo <nicoo@debian.org>  Tue, 13 Feb 2018 19:37:14 +0100

mblaze (0.3.1-1) unstable; urgency=medium

  * New upstream version (2018-01-30)
    * New tools mbnc, mflow, mraw, mrecode
    * Numerous bug fixes
    * Improved documentation and UTF-8 parsing
    * mcolor, mless now support $NO_COLOR
    * blaze822.h ensures PATH_MAX is defined (Closes: 888398)

  * Update Lintian overrides
    * Add debian-watch-could-verify-download
      We use signed Git tags from upstream, not signed tarballs
    * Add hardening-no-fortify-functions due to a false negative

  * Update debian/copyright
    * Refer to the CC0-1.0 license file under /usr/share/common-licenses
    * Use a HTTPS URI for the DEP5 format

  * Update debian/control
    * Conflict with msort, mpdtoys (Closes: 887988)
    * Fix the formatting in Description (Closes: 888083)
    * Declare compliance with policy 4.1.3
      No change required.
    * Switch to debhelper 11

  * Install upstream's changelog

 -- nicoo <nicoo@debian.org>  Wed, 31 Jan 2018 18:58:56 +0100

mblaze (0.2-1) unstable; urgency=medium

  * Initial packaging (Closes: 882788)

 -- nicoo <nicoo@debian.org>  Mon, 30 Oct 2017 18:29:10 +0100
