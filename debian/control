Source: mblaze
Maintainer: nicoo <nicoo@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Homepage: https://git.vuxu.org/mblaze/about/
Vcs-Browser: https://salsa.debian.org/debian/mblaze
Vcs-Git: https://salsa.debian.org/debian/mblaze.git


Package: mblaze
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: UNIX utilities to deal with Maildir
 The mblaze message system is a set of Unix utilities to deal with mail kept in
 Maildir folders. It is a classic command line MUA and has no features for
 receiving or transferring mail; you are expected to fetch your mail using
 fdm(1), getmail(1) offlineimap(1), procmail(1), or similar , and send it using
 dma(8), msmtp(1), sendmail(8), as provided by OpenSMTPD, Postfix, or similar.
 mblaze expects your mail to reside in Maildir folders.
 .
 Its design is roughly inspired by MH, the RAND Message Handling System, but it
 is a complete implementation from scratch. mblaze operates directly on Maildir
 folders and doesn't use its own caches or databases. There is no setup needed
 for many uses.
 .
 mblaze consists of these Unix tools that each do one job:
  * maddr(1)     extract addresses from mail
  * magrep(1)    find mails matching a pattern
  * mcom(1)      compose and send mail
  * mdeliver(1)  deliver messages or import mailboxes
  * mdirs(1)     find Maildir folders
  * mexport(1)   export Maildir folders as mailboxes
  * mflag(1)     change flags (marks) of mail
  * mflow(1)     reflow format=flowed plain text mails
  * mfwd(1)      forward mail
  * mgenmid(1)   generate Message-IDs
  * mhdr(1)      extract mail headers
  * minc(1)      incorporate new mail
  * mless(1)     conveniently read mail in less(1)
  * mlist(1)     list and filter mail messages
  * mmime(1)     create MIME messages
  * mmkdir(1)    create new Maildir
  * mpick(1)     advanced mail filter
  * mrep(1)      reply to mail
  * mscan(1)     generate one-line summaries of mail
  * msed(1)      manipulate mail headers
  * mseq(1)      manipulate mail sequences
  * mshow(1)     render mail and extract attachments
  * msort(1)     sort mail
  * mthread(1)   arrange mail into discussions
